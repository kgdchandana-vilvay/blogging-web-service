package com.vilvay.blogging.web.service.controller;

import com.vilvay.blogging.web.service.dto.Author;
import com.vilvay.blogging.web.service.dto.Post;
import com.vilvay.blogging.web.service.service.AuthorService;
import com.vilvay.blogging.web.service.service.PostService;
import com.vilvay.blogging.web.service.util.AuthorValidationUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Author controller.
 */
@RestController
@RequestMapping("/api/author")
@Slf4j
@Api(value = "Handles all Author related API requests")
public class AuthorController {

  @Autowired
  private AuthorService authorService;
  @Autowired
  private PostService postService;
  @Autowired
  private AuthorValidationUtil authorValidationUtil;

  /**
   * Save author author.
   *
   * @param author the author
   * @return the author
   */
  @ApiOperation(value = "Save author")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully Save author", response = Author.class)})
  @PostMapping("/")
  public ResponseEntity<Author> saveAuthor(@RequestBody Author author) {
    log.info("Save Author: {}", author);
    return new ResponseEntity<>(authorService.saveAuthor(author), HttpStatus.OK);
  }

  /**
   * Gets authors.
   *
   * @return the authors
   */
  @ApiOperation(value = "Gets authors")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully Gets authors", response = Author.class)})
  @GetMapping("/")
  public ResponseEntity<List<Author>> getAuthors() {
    log.info("get All authors");
    return new ResponseEntity<>(authorService.getAllAuthor(), HttpStatus.OK);
  }

  /**
   * Gets post by id.
   *
   * @param id the id
   * @return the post by id
   */
  @ApiOperation(value = "Gets post by id")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully Gets post by id", response = Author.class)})
  @GetMapping("/{id}")
  public ResponseEntity<Author> getPostById(@PathVariable("id") Long id) {
    log.info("find author by Id: {}", id);
    return new ResponseEntity<>(authorService.getAuthorById(id), HttpStatus.OK);
  }

  /**
   * Update author author.
   *
   * @param id     the id
   * @param author the author
   * @return the author
   */
  @ApiOperation(value = "Update author")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully Update author", response = Author.class)})
  @PutMapping("/{id}")
  @PreAuthorize("hasAuthority('ROLE_USER') or hasAuthority('ROLE_ADMIN')")
  public ResponseEntity<Author> updateAuthor(@PathVariable("id") Long id, @RequestBody Author author) {
    log.info("updating Id : {}, Author : {}", author, id);
    authorValidationUtil.validateAuthorRequestBodyAndId(author, id);
    authorValidationUtil.validateAuthorRequestBodyAndId(author, id);
    log.info("Author Updating validation Success");
    return new ResponseEntity<>(authorService.updateAuthor(id, author), HttpStatus.OK);
  }

  /**
   * Delete author string.
   *
   * @param id the id
   * @return the string
   */
  @ApiOperation(value = "Delete author")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully Delete author", response = String.class)})
  @DeleteMapping("/{id}")
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  public ResponseEntity<String> deleteAuthor(@PathVariable("id") Long id) {
    return new ResponseEntity<>(authorService.deleteAuthor(id), HttpStatus.OK);
  }

  /**
   * Gets posts by author id.
   *
   * @param id the id
   * @return the posts by author id
   */
  @ApiOperation(value = "Gets posts by author id")
  @ApiResponses(
      value = {@ApiResponse(code = 200, message = "Successfully Gets posts by author id", response = Post[].class)})
  @GetMapping("{id}/posts")
  public ResponseEntity<List<Post>> getPostsByAuthorId(@PathVariable("id") Long id) {
    log.info("get All posts");
    return new ResponseEntity<>(postService.getPostByAuthorId(id), HttpStatus.OK);
  }
}
