package com.vilvay.blogging.web.service.service;

import com.vilvay.blogging.web.service.dto.Author;
import com.vilvay.blogging.web.service.error.BadRequestException;
import com.vilvay.blogging.web.service.error.ResourceNotFoundException;
import com.vilvay.blogging.web.service.mapper.DtoDAOMappers;
import com.vilvay.blogging.web.service.repository.AuthorRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorService {

  @Autowired
  private AuthorRepository authorRepository;

  public Author saveAuthor(Author author) {
    Optional<com.vilvay.blogging.web.service.entity.Author> optionalAuthor =
            authorRepository.findByUsername(author.getUsername());
    if (optionalAuthor.isPresent()) {
      throw new BadRequestException("Username already exist - ", author.getUsername());
    }
    com.vilvay.blogging.web.service.entity.Author authorDao = DtoDAOMappers.INSTANCE.authorDtoToTAO(author);
    return DtoDAOMappers.INSTANCE.authorDaoToDTO(authorRepository.save(authorDao));
  }

  public List<Author> getAllAuthor() {
    return DtoDAOMappers.INSTANCE.authorDaoToDTO(authorRepository.findAll());
  }

  public Author getAuthorById(Long id) {
    Optional<com.vilvay.blogging.web.service.entity.Author> optionalAuthor = authorRepository.findById(id);
    if (optionalAuthor.isPresent()) {
      return DtoDAOMappers.INSTANCE.authorDaoToDTO(optionalAuthor.get());
    }
    throw new ResourceNotFoundException("404_NotFound: author not found for id - " + id);
  }

  public Author updateAuthor(Long id, Author author) {
    Optional<com.vilvay.blogging.web.service.entity.Author> optionalAuthor = authorRepository.findById(id);
    if (optionalAuthor.isEmpty()) {
      throw new ResourceNotFoundException("404_NotFound author not found for id - " + id);
    }
    com.vilvay.blogging.web.service.entity.Author authorDao = DtoDAOMappers.INSTANCE.authorDtoToTAO(author);
    return DtoDAOMappers.INSTANCE.authorDaoToDTO(authorRepository.save(authorDao));
  }

  public String deleteAuthor(Long id) {
    authorRepository.deleteById(id);
    return "Delete author successfully!!!";
  }
}
