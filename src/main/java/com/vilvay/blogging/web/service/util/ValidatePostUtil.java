package com.vilvay.blogging.web.service.util;

import com.vilvay.blogging.web.service.dto.Post;
import com.vilvay.blogging.web.service.error.BadRequestException;
import java.util.Objects;
import org.springframework.stereotype.Component;

@Component
public class ValidatePostUtil {

  public void validatePostRequestBodyAndId(Post post, Long id) {
    if (Objects.isNull(id) || Objects.isNull(post) || !id.equals(post.getId())) {
      throw new BadRequestException("Post Id and url id mismatching");
    }
  }
}
