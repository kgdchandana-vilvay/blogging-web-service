package com.vilvay.blogging.web.service.mapper;

import com.vilvay.blogging.web.service.dto.Author;
import com.vilvay.blogging.web.service.dto.Comment;
import com.vilvay.blogging.web.service.dto.Post;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface DtoDAOMappers {

  DtoDAOMappers INSTANCE = Mappers.getMapper(DtoDAOMappers.class);

  @Mapping(target = ".", source = "author")
  com.vilvay.blogging.web.service.entity.Author authorDtoToTAO(Author author);

  @Mapping(target = ".", source = "author")
  Author authorDaoToDTO(com.vilvay.blogging.web.service.entity.Author author);

  @Mapping(target = ".", source = "authors")
  List<Author> authorDaoToDTO(List<com.vilvay.blogging.web.service.entity.Author> authors);

  @Mapping(target = ".", source = "post")
  Post postDaoToDTO(com.vilvay.blogging.web.service.entity.Post post);

  @Mapping(target = ".", source = "post")
  com.vilvay.blogging.web.service.entity.Post PostDtoToDAO(Post post);

  @Mapping(target = ".", source = "posts")
  List<Post> postDaoToDTO(List<com.vilvay.blogging.web.service.entity.Post> posts);

  @Mapping(target = ".", source = "comment")
  Comment commentDaoToDTO(com.vilvay.blogging.web.service.entity.Comment comment);

  @Mapping(target = ".", source = "comment")
  com.vilvay.blogging.web.service.entity.Comment commentDtoToDAO(Comment comment);

  @Mapping(target = ".", source = "comments")
  List<Comment> commentDaoToDTO(List<com.vilvay.blogging.web.service.entity.Comment> comments);
}
