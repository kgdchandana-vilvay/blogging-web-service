package com.vilvay.blogging.web.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.sql.Timestamp;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Post {
  private Long id;
  private String title;
  private String body;
  private Long authorId;
  private Timestamp createdOn;
  private Timestamp modifiedOn;
  private List<Comment> comments;
}
