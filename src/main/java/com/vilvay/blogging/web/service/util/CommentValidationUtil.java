package com.vilvay.blogging.web.service.util;

import com.vilvay.blogging.web.service.dto.Comment;
import com.vilvay.blogging.web.service.error.BadRequestException;
import java.util.Objects;
import org.springframework.stereotype.Component;

@Component
public class CommentValidationUtil {

  public void validateCommentRequestBodyAndId(Comment comment, Long id) {
    if (Objects.isNull(id) || Objects.isNull(comment) || !id.equals(comment.getId())) {
      throw new BadRequestException("Comment Id and url id mismatching");
    }
  }
}
