package com.vilvay.blogging.web.service.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix = "app.auth-service")
public class ConfigUtils {
  private String registrationUrl;
  private String loginUrl;
  private String validateUrl;
  private String refreshTokenUrl;
  private String basicAuth;
}
