package com.vilvay.blogging.web.service.config.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidateResponse {

  private String userId;
  @JsonProperty("user_name")
  private String userName;
  private String roles;
  private List<String> authorities;

  public String getRoles() {
    if (authorities != null) {

      return authorities.stream().collect(Collectors.joining(","));
    }
    return null;
  }
}
