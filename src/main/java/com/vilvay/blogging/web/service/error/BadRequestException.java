package com.vilvay.blogging.web.service.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException implements CustomError {
  private final String message;
  private String code;

  public BadRequestException(String message) {
    super(message);
    this.message = message;
  }

  public BadRequestException(String message, String code) {
    super(message);
    this.message = message;
    this.code = code;
  }

  public BadRequestException(String message, Throwable throwable) {
    super(message, throwable);
    this.message = message;
  }

  public BadRequestException(String message, String code, Throwable throwable) {
    super(message, throwable);
    this.message = message;
    this.code = code;
  }

  public String getCode() {
    return this.code;
  }

  public String getMessage() {
    return this.message;
  }
}
