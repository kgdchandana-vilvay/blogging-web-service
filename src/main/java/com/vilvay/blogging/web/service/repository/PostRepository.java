package com.vilvay.blogging.web.service.repository;

import com.vilvay.blogging.web.service.entity.Post;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

  List<Post> findPostByAuthorId(Long authorId);
}
