package com.vilvay.blogging.web.service.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Author {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "auth_id_seq_generator")
  @SequenceGenerator(name = "auth_id_seq_generator", sequenceName = "auth_id_seq", initialValue = 1)
  private Long id;
  private String name;
  @Column(nullable = false, unique = true)
  private String username;
  private String email;
  private String address;
  @OneToMany(cascade = {CascadeType.ALL})
  @JoinColumn(name = "authorId", nullable = false, insertable = false, updatable = false)
  private List<Post> posts;
}
