package com.vilvay.blogging.web.service.controller;

import com.vilvay.blogging.web.service.dto.Comment;
import com.vilvay.blogging.web.service.dto.Post;
import com.vilvay.blogging.web.service.service.CommentService;
import com.vilvay.blogging.web.service.util.CommentValidationUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Comment controller.
 */
@RestController
@RequestMapping("/api/comment")
@Slf4j
@Api(value = "Handles all Comment related API requests")
public class CommentController {

  @Autowired
  private CommentService commentService;
  @Autowired
  private CommentValidationUtil commentValidationUtil;

  /**
   * Save comment comment.
   *
   * @param comment the comment
   * @return the comment
   */
  @ApiOperation(value = "Save comment")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully Save comment", response = Comment.class)})
  @PostMapping("/")
  public ResponseEntity<Comment> saveComment(@RequestBody Comment comment) {
    log.info("Save Post: {}", comment);
    return new ResponseEntity<>(commentService.saveComment(comment), HttpStatus.OK);
  }

  /*
   * Seems this is not practical scenario
   *
   * @param id the id
   * @return the comment by id
   * @GetMapping("/") public List<Comment> getComments() { log.info("get All comments"); return
   * commentService.getAllComment(); }
   */

  /**
   * Gets comment by id.
   *
   * @param id the id
   * @return the comment by id
   */
  @ApiOperation(value = "Gets comment by id")
  @ApiResponses(
      value = {@ApiResponse(code = 200, message = "Successfully Gets comment by id", response = Comment.class)})
  @GetMapping("/{id}")
  public ResponseEntity<Comment> getCommentById(@PathVariable("id") Long id) {
    log.info("find Comment by Id: {}", id);
    return new ResponseEntity<>(commentService.getCommentById(id), HttpStatus.OK);
  }

  /**
   * Delete comment string.
   *
   * @param id the id
   * @return the string
   */
  @ApiOperation(value = "Delete comment")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully Delete comment", response = String.class)})
  @DeleteMapping("/{id}")
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  public ResponseEntity<String> deleteComment(@PathVariable("id") Long id) {
    return new ResponseEntity<>(commentService.deleteComment(id), HttpStatus.OK);
  }

  /**
   * Update comment comment.
   *
   * @param id      the id
   * @param comment the comment
   * @return the comment
   */
  @ApiOperation(value = "Update comment")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully Update comment", response = Comment.class)})
  @PutMapping("/{id}")
  @PreAuthorize("hasAuthority('ROLE_USER') or hasAuthority('ROLE_ADMIN')")
  public ResponseEntity<Comment> updateComment(@PathVariable("id") Long id, @RequestBody Comment comment) {
    log.info("updating Id : {}, Comment : {}", comment, id);
    commentValidationUtil.validateCommentRequestBodyAndId(comment, id);
    log.info("comment Updating validation Success");
    return new ResponseEntity<>(commentService.updateComment(id, comment), HttpStatus.OK);
  }
}
