package com.vilvay.blogging.web.service.error;

public interface CustomError {
  String getCode();

  String getMessage();
}
