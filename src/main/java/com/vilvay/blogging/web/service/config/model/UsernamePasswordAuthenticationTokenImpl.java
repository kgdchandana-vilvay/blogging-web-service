package com.vilvay.blogging.web.service.config.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

@Setter
@Getter
public class UsernamePasswordAuthenticationTokenImpl extends UsernamePasswordAuthenticationToken {

  private String token;

  public UsernamePasswordAuthenticationTokenImpl(String token) {
    super(null, null);
    this.token = token;
  }
}
