package com.vilvay.blogging.web.service.service;

import com.vilvay.blogging.web.service.dto.Post;
import com.vilvay.blogging.web.service.error.ResourceNotFoundException;
import com.vilvay.blogging.web.service.mapper.DtoDAOMappers;
import com.vilvay.blogging.web.service.repository.PostRepository;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostService {

  @Autowired
  private PostRepository postRepository;

  @Transactional
  public Post savePost(Post post) {
    com.vilvay.blogging.web.service.entity.Post postDao = DtoDAOMappers.INSTANCE.PostDtoToDAO(post);
    return DtoDAOMappers.INSTANCE.postDaoToDTO(postRepository.save(postDao));
  }

  public List<Post> getAllPost() {
    return DtoDAOMappers.INSTANCE.postDaoToDTO(postRepository.findAll());
  }

  public Post getPostById(Long id) {
    Optional<com.vilvay.blogging.web.service.entity.Post> optionalPost = postRepository.findById(id);
    if (optionalPost.isPresent()) {
      return DtoDAOMappers.INSTANCE.postDaoToDTO(optionalPost.get());
    }
    throw new ResourceNotFoundException("404_NotFound post not found for id - " + id);
  }

  public Post updatePost(Long id, Post post) {
    Optional<com.vilvay.blogging.web.service.entity.Post> optionalPost = postRepository.findById(id);
    if (!optionalPost.isPresent()) {
      throw new ResourceNotFoundException("404_NotFound post not found for id - " + id);
    }
    com.vilvay.blogging.web.service.entity.Post postDao = DtoDAOMappers.INSTANCE.PostDtoToDAO(post);
    return DtoDAOMappers.INSTANCE.postDaoToDTO(postRepository.save(postDao));
  }

  public List<Post> getPostByAuthorId(Long authorId) {
    return DtoDAOMappers.INSTANCE.postDaoToDTO(postRepository.findPostByAuthorId(authorId));
  }

  public String deletePost(Long id) {
    postRepository.deleteById(id);
    return "Delete post successfully!!!";
  }
}
