package com.vilvay.blogging.web.service.service;

import com.vilvay.blogging.web.service.dto.Comment;
import com.vilvay.blogging.web.service.error.ResourceNotFoundException;
import com.vilvay.blogging.web.service.mapper.DtoDAOMappers;
import com.vilvay.blogging.web.service.repository.CommentRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentService {

  @Autowired
  CommentRepository commentRepository;

  public Comment saveComment(Comment comment) {

    com.vilvay.blogging.web.service.entity.Comment commentDao = DtoDAOMappers.INSTANCE.commentDtoToDAO(comment);
    return DtoDAOMappers.INSTANCE.INSTANCE.commentDaoToDTO(commentRepository.save(commentDao));
  }

  public Comment getCommentById(Long id) {
    Optional<com.vilvay.blogging.web.service.entity.Comment> optionalComment = commentRepository.findById(id);
    if (optionalComment.isPresent()) {
      return DtoDAOMappers.INSTANCE.INSTANCE.commentDaoToDTO(optionalComment.get());
    }
    throw new ResourceNotFoundException("404_NotFound comment not found for id - " + id);
  }

  public Comment updateComment(Long id, Comment comment) {

    Optional<com.vilvay.blogging.web.service.entity.Comment> optionalComment = commentRepository.findById(id);
    if (!optionalComment.isPresent()) {
      throw new ResourceNotFoundException("404_NotFound: comment not found for id - " + id);
    }
    com.vilvay.blogging.web.service.entity.Comment commentDao = DtoDAOMappers.INSTANCE.commentDtoToDAO(comment);
    return DtoDAOMappers.INSTANCE.INSTANCE.commentDaoToDTO(commentRepository.save(commentDao));
  }

  public List<Comment> getCommentByPostId(Long id) {
    return DtoDAOMappers.INSTANCE.INSTANCE.commentDaoToDTO(commentRepository.findCommentByPostId(id));
  }

  public String deleteComment(Long id) {
    commentRepository.deleteById(id);
    return "Delete comment successfully!!!";
  }
}
