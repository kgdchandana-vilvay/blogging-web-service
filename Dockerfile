FROM amazoncorretto:11
EXPOSE 9001
ADD target/blogging-web-service.jar blogging-web-service.jar
ENTRYPOINT ["java","-jar","/blogging-web-service.jar"]