package com.vilvay.blogging.web.service.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@EntityListeners(AuditingEntityListener.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Comment {

  @javax.persistence.Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "comment_id_seq_generator")
  @SequenceGenerator(name = "comment_id_seq_generator", sequenceName = "comment_id_seq")
  private Long id;
  private Long postId;
  private String name;
  private String email;
  private String body;
  @CreatedDate
  @Column(nullable = false, updatable = false)
  private Timestamp createdOn;
  @LastModifiedDate
  @Column(insertable = false)
  private Timestamp modifiedOn;
}
