package com.vilvay.blogging.web.service.util;

import com.vilvay.blogging.web.service.dto.Author;
import com.vilvay.blogging.web.service.error.BadRequestException;
import java.util.Objects;
import org.springframework.stereotype.Component;

@Component
public class AuthorValidationUtil {

  public void validateAuthorRequestBodyAndId(Author author, Long id) {
    if (Objects.isNull(id) || Objects.isNull(author) || !id.equals(author.getId())) {
      throw new BadRequestException("Author Id and url id mismatching");
    }
  }

}
