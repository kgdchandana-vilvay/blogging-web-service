package com.vilvay.blogging.web.service.repository;

import com.vilvay.blogging.web.service.entity.Comment;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

  List<Comment> findCommentByPostId(Long id);
}
