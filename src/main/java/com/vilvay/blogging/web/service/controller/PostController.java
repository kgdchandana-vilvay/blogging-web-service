package com.vilvay.blogging.web.service.controller;

import com.sun.istack.NotNull;
import com.vilvay.blogging.web.service.dto.Comment;
import com.vilvay.blogging.web.service.dto.Post;
import com.vilvay.blogging.web.service.service.CommentService;
import com.vilvay.blogging.web.service.service.PostService;
import com.vilvay.blogging.web.service.util.ValidatePostUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Post controller.
 */
@RestController
@RequestMapping("/api/post")
@Slf4j
@Api(value = "Handles all Post related API requests")
public class PostController {

  @Autowired
  private PostService postService;
  @Autowired
  private CommentService commentService;
  @Autowired
  private ValidatePostUtil validatePost;

  /**
   * Save post post.
   *
   * @param post the post
   * @return the post
   */
  @ApiOperation(value = "Save post")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully Save post", response = Post.class)})
  @PostMapping("/")
  public ResponseEntity<Post> savePost(@RequestBody Post post) {
    log.info("Save Post: {}", post);
    return new ResponseEntity<>(postService.savePost(post), HttpStatus.OK);
  }

  /**
   * Gets posts.
   *
   * @return the posts
   */
  @ApiOperation(value = "Gets posts")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully Gets posts", response = Post[].class)})
  @GetMapping("/")
  public ResponseEntity<List<Post>> getPosts() {
    log.info("get All posts");
    return new ResponseEntity<>(postService.getAllPost(), HttpStatus.OK);
  }

  /**
   * Gets post by id.
   *
   * @param id the id
   * @return the post by id
   */
  @ApiOperation(value = "Gets post by id")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully Gets post by id", response = Post.class)})
  @GetMapping("/{id}")
  public ResponseEntity<Post> getPostById(@NotNull @PathVariable("id") Long id) {
    log.info("find Post by Id: {}", id);
    return new ResponseEntity<>(postService.getPostById(id), HttpStatus.OK);
  }

  /**
   * Update post post.
   *
   * @param id   the id
   * @param post the post
   * @return the post
   */
  @ApiOperation(value = "Update post")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully Update post", response = Post.class)})
  @PutMapping("/{id}")
  @PreAuthorize("hasAuthority('ROLE_USER') or hasAuthority('ROLE_ADMIN')")
  public ResponseEntity<Post> updatePost(@NotNull @PathVariable("id") Long id, @RequestBody Post post) {
    log.info("updating Id : {}, Post : {}", post, id);
    validatePost.validatePostRequestBodyAndId(post, id);
    log.info("Post Updating validation Success");
    return new ResponseEntity<>(postService.updatePost(id, post), HttpStatus.OK);
  }

  /**
   * Delete post string.
   *
   * @param id the id
   * @return the string
   */
  @ApiOperation(value = "Delete post")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully Delete post", response = String.class)})
  @DeleteMapping("/{id}")
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  public ResponseEntity<String> deletePost(@NotNull @PathVariable("id") Long id) {
    return new ResponseEntity<>(postService.deletePost(id), HttpStatus.OK);
  }

  /**
   * Gets comments by post id.
   *
   * @param id the id
   * @return the comments by post id
   */
  @ApiOperation(value = "Gets comments by post id")
  @ApiResponses(
      value = {@ApiResponse(code = 200, message = "Successfully Gets comments by post id", response = Comment[].class)})
  @GetMapping("{id}/comments")
  public ResponseEntity<List<Comment>> getCommentsByPostId(@NotNull @PathVariable("id") Long id) {
    log.info("get All posts");
    return new ResponseEntity<>(commentService.getCommentByPostId(id), HttpStatus.OK);
  }
}
