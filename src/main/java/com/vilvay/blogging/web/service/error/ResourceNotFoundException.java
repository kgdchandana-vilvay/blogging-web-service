package com.vilvay.blogging.web.service.error;

public class ResourceNotFoundException extends RuntimeException implements CustomError {
  private final String message;
  private String code;

  public ResourceNotFoundException(String message) {
    super(message);
    this.message = message;
  }

  public ResourceNotFoundException(String message, String code) {
    super(message);
    this.message = message;
    this.code = code;
  }

  public ResourceNotFoundException(String message, String code, Throwable throwable) {
    super(message, throwable);
    this.message = message;
    this.code = code;
  }

  public String getCode() {
    return this.code;
  }

  public String getMessage() {
    return this.message;
  }
}
