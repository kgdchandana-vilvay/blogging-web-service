package com.vilvay.blogging.web.service.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@EntityListeners(AuditingEntityListener.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Post {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "post_id_seq_generator")
  @SequenceGenerator(name = "post_id_seq_generator", sequenceName = "post_id_seq")
  private Long id;
  private String title;
  private String body;
  private Long authorId;
  @CreatedDate
  @Column(nullable = false, updatable = false)
  private Timestamp createdOn;
  @LastModifiedDate
  @Column(insertable = false)
  private Timestamp modifiedOn;
  @OneToMany(cascade = {CascadeType.ALL})
  @JoinColumn(name = "postId", nullable = false, insertable = false, updatable = false)
  private List<Comment> comments;
}
