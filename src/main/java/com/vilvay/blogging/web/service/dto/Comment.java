package com.vilvay.blogging.web.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Comment {
  private Long id;
  private Long postId;
  private String name;
  private String email;
  private String body;
  private Timestamp createdOn;
  private Timestamp modifiedOn;
}
