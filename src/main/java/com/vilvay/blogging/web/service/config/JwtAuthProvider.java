package com.vilvay.blogging.web.service.config;

import com.vilvay.blogging.web.service.config.model.UserDetailsImpl;
import com.vilvay.blogging.web.service.config.model.UsernamePasswordAuthenticationTokenImpl;
import com.vilvay.blogging.web.service.config.model.ValidateResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class JwtAuthProvider extends AbstractUserDetailsAuthenticationProvider {
  @Autowired
  private ConfigUtils configUtils;
  @Autowired
  private RestTemplate restTemplate;

  @Override
  @SneakyThrows
  protected void additionalAuthenticationChecks(UserDetails userDetails,
      UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    // NO-OP required
  }

  @Override
  @SneakyThrows
  protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
      throws AuthenticationException {
    try {
      UsernamePasswordAuthenticationTokenImpl token = (UsernamePasswordAuthenticationTokenImpl) authentication;
      MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
      headers.add(HttpHeaders.AUTHORIZATION, configUtils.getBasicAuth());
      UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(configUtils.getValidateUrl())
          .queryParam("token", token.getToken().replace("Bearer ", ""));
      HttpEntity<UsernamePasswordAuthenticationTokenImpl> entity = new HttpEntity<>(headers);
      ValidateResponse resData = restTemplate.exchange(builder.build().toUri(), HttpMethod.POST,
              entity, ValidateResponse.class).getBody();

      List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList(resData.getRoles());
      return new UserDetailsImpl(resData.getUserName(),
//          resData.getUserId(),
          grantedAuthorities);
    } catch (Exception exception){
      throw new RuntimeException("Invalid AccessToken");
    }
  }
}
